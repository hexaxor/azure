#!/bin/bash

help()
{
    echo "
Usage: iprotate_azure.sh
               [ -e | --static_eth     ]
               [ -d | --dynamic_eth    ]
               [ -r | --resource_group ]
               [ -i | --ipname         ]
               [ -v | --vmname         ]
               [ -c | --ipconfig       ]
               [ -n | --nic            ]
               [ -p | --port           ]
               [ -a | --privateip      ]
               [ -h | --help           ]"
    exit 2
}


SHORT=e:,d:,r:,i:,v:,c:,n:,p:,a:,h
LONG=static_eth:,dynamic_eth:,resource_group:,ipname:,vmname:,ipconfig:,nic:,port:,privateip:,help
OPTS=$(getopt --alternative --name iprotate_azure --options $SHORT --longoptions $LONG -- "$@")

eval set -- "$OPTS"

while :
do
  case "$1" in
    -e | --static_eth )
      static_eth="$2"
      shift 2
      ;;
    -d | --dynamic_eth )
      dynamic_eth="$2"
      shift 2
      ;;
    -r | --resource_group )
      resource_group="$2"
      shift 2
      ;;
    -i | --ipname )
      ipname="$2"
      shift 2
      ;;
    -v | --vmname )
      vmname="$2"
      shift 2
      ;;
    -c | --ipconfig )
      ipconfig="$2"
      shift 2
      ;;
    -n | --nic )
      nic="$2"
      shift 2
      ;;
    -p | --port )
      port="$2"
      shift 2
      ;;
    -a | --privateip )
      privateip="$2"
      shift 2
      ;;
    -h | --help)
      help
      ;;
    --)
      shift;
      break
      ;;
    *)
      echo "Unexpected option: $1"
      ;;
  esac
done

privateiponly=$(echo $privateip | cut -d "/" -f 1)
gateway=$(ip route show | grep "dhcp" | cut -d " " -f 3 | head -1)

echo "Starting Process Setup Ip Rotate Azure"
sleep 5

sudo add-apt-repository ppa:deadsnakes/ppa -y
curl https://gitlab.com/hexaxor/azure/-/raw/main/danted.sh?inline=false | bash -s -- $static_eth $dynamic_eth $port
apt install jq nload net-tools -y
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

wget https://gitlab.com/hexaxor/azure/-/raw/main/60-static.yaml?inline=false -O /etc/netplan/60-static.yaml

sed -i "s|eth1|$dynamic_eth|g" "/etc/netplan/60-static.yaml"
sed -i "s|ip|"$privateip"|g" "/etc/netplan/60-static.yaml"

netplan apply

echo 150 custom >> /etc/iproute2/rt_tables 
ip rule add from $privateiponly lookup custom
ip route add default via $gateway dev $dynamic_eth table custom

cd /root

apt update
apt install python3.8
wget -O /root/iprotate.tar.gz https://gitlab.com/hexaxor/azure/-/raw/main/iprotate.tar.gz?inline=false
tar -xf iprotate.tar.gz

nl=$'\n'
echo "source /root/iprotate/bin/activate${nl}python /root/iprotate/api.py" > /root/run_api
screen -dmS api bash /root/run_api

newip_script="/usr/bin/newip-azure"
wget https://gitlab.com/hexaxor/azure/-/raw/main/newip-azure?inline=false -O $newip_script && chmod +x $newip_script

sed -i "s|resource_group=|resource_group=$resource_group|g" "$newip_script"
sed -i "s|vmname=|vmname=$vmname|g" "$newip_script"
sed -i "s|nic=|nic=$nic|g" "$newip_script"
sed -i "s|ipconfig=|ipconfig=$ipconfig|g" "$newip_script"
sed -i "s|ipname=|ipname=$ipname|g" "$newip_script"
